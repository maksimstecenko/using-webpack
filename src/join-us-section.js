const appInputFormJoinOurProgram=document.createElement('form')

const appInputFormInputEmailJoinOurProgram=document.createElement('INPUT')
appInputFormInputEmailJoinOurProgram.setAttribute('type','email')

function createSectionJoinOurProgram () {
    const appSectionAppSectionImageCulture=document.querySelectorAll('section')[2];
    
    const joinOurProgram=document.createElement('section')
    
    joinOurProgram.className='app-section_join-our-program'
    
    const appTextJoinOurProgram=document.createElement(`p`)
    
    appTextJoinOurProgram.className='app-text_join-our-program'
    
    appTextJoinOurProgram.innerHTML= `Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.`
    
    appSectionAppSectionImageCulture.after(joinOurProgram)
    
    joinOurProgram.innerHTML=`<h3 class="app-header_join-our-program">Join Our Program</h3>`
    
    const appHeaderJoinOurProgram=document.querySelector('.app-header_join-our-program')
    
    appHeaderJoinOurProgram.after(appTextJoinOurProgram)
    
    /*const appInputFormJoinOurProgram=document.createElement('form')*/
    
    appInputFormJoinOurProgram.className='app-form_join-our-program'
    
    appTextJoinOurProgram.after(appInputFormJoinOurProgram)
    
    const appInputFormInputEmailJoinOurProgram=document.createElement('INPUT')
    appInputFormInputEmailJoinOurProgram.setAttribute('type','email')
    
    appInputFormInputEmailJoinOurProgram.className='app-form-input-email_join-our-program'
    
    appInputFormJoinOurProgram.prepend(appInputFormInputEmailJoinOurProgram)
    
    const appInputFormSubmitButtonJoinOurProgram=document.createElement('INPUT')
    appInputFormSubmitButtonJoinOurProgram.setAttribute('type','submit')
    
    appInputFormSubmitButtonJoinOurProgram.className='app-form-input-submit_join-our-program'
    
    appInputFormJoinOurProgram.append(appInputFormSubmitButtonJoinOurProgram)
    
    joinOurProgram.style.cssText=`
    background: url("assets/images/joinOurProject.jpg");
    display:flex;
    background-size: cover;
    flex-direction:column;
    width: 1200px;
    height: 440px;
    `
    
    appHeaderJoinOurProgram.style.cssText=`
    width: 480px;
    height: 47px;
    margin-left: 357px;
    margin-top: 85px;
    color: black;
    text-align: center;
    font-family: Oswald bold;
    font-size: 48px;
    font-style: normal;
    font-weight: 700;
    line-height: 64px;
    `
    
    appTextJoinOurProgram.style.cssText=`
    color: black;
    text-align: center;
    font-family: Source Sans Pro;
    font-size: 24px;
    font-style: normal;
    font-weight: 400;
    line-height: 32px;
    width: 452px;
    height: 54px;
    margin-left: 371px;
    margin-top: 14px;
    `
    
    appInputFormInputEmailJoinOurProgram.style.cssText=`
    width: 400px;
    height: 36px;
    margin-left: 307px;
    margin-top: 27px;
    opacity: 0.33;
    `
    
    appInputFormSubmitButtonJoinOurProgram.style.cssText=`
    width: 111px;
    height: 36px;
    border-radius: 18px;
    background: #55C2D8;
    margin-left: 29px;
    `}

function SectionCreator() {}

SectionCreator.prototype.create = function(type) {
  let section;
  if (type === 'standard') {
    section = new StandardProgramSection();
  }
  
  else if (type === 'advanced') {
    section = new AdvancedProgramSection();
  }

   section.remove = function() {
    section.element.parentNode.removeChild(section.element);
  };

  return section;
};

function StandardProgramSection() {
  this.element = document.createElement('section');
  this.element.innerHTML = '<h2>Join Our Program</h2><p>Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.</p><button>Subscribe</button>';
}

function AdvancedProgramSection() {
  this.element = document.createElement('div');
  this.element.innerHTML = '<h2>Join Our Advanced Program</h2><p>Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.</p><button>Subscribe to Advanced Program</button>';
}

    export {appInputFormJoinOurProgram, appInputFormInputEmailJoinOurProgram, createSectionJoinOurProgram};